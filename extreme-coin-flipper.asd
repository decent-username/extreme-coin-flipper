;;;; extreme-coin-flipper.asd

(asdf:defsystem #:extreme-coin-flipper
  :description "Describe extreme-coin-flipper here"
  :author "Your Name <your.name@example.com>"
  :license  "Specify license here"
  :version "0.0.1"
  :serial t
  :depends-on (trivial-gamekit)
  :pathname "src/"
  :components ((:file "package")
               (:file "extreme-coin-flipper")))
